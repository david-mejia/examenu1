const form = document.getElementById("coctel-form");
const cocktailName = document.getElementById("nombre");
const cocktailInfo = document.getElementById("coctel-info");
const clearButton = document.getElementById("clear-button");

form.addEventListener("submit", (e) => {
    e.preventDefault();
    const drinkstxtNombre = cocktailName.value.trim();
    if (drinkstxtNombre) {
        axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${drinkstxtNombre}`)
            .then((response) => {
                const cocktail = response.data.drinks[0];
                cocktailInfo.innerHTML = `
                    <h2>${cocktail.strDrink}</h2>
                    <p>Categoria: ${cocktail.strCategory}</p>
                    <p>Instruciones: ${cocktail.strInstructions}</p>
                    <img src="${cocktail.strDrinkThumb}" alt="${cocktail.strDrink}">
                `;
            })
            .catch((error) => {
                console.error(error);
                cocktailInfo.innerHTML = "<p>Error: Coctel no encontrado</p>";
            });
    } else {
        cocktailInfo.innerHTML = "<p>Por favor ingrese el nombre del coctel</p>";
    }
});

clearButton.addEventListener("click", () => {
    cocktailName.value = "";
    cocktailInfo.innerHTML = "";
});